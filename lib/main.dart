import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: FirstPage(),
    );
  }
}

class FirstPage extends StatefulWidget {
  @override
  _FirstPage createState() => _FirstPage();
}

class _FirstPage extends State<FirstPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "hitung skor!",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 36, color: Colors.orange[900]),
              ),
              SizedBox(   //Use of SizedBox 
                height: 100, 
              ),
              Text(
                "TEAM 1",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.orange[900]),
              ),
              SizedBox(   //Use of SizedBox 
                height: 5, 
              ),
              Container(
                width: 300.0,
                child: TextField(
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      borderSide: BorderSide(width: 1, color: Colors.deepOrange),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      borderSide: BorderSide(width: 1, color: Colors.deepOrange),
                    ),
                  ),                                 
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.orange[900],                  
                  )
                )
              ),
              SizedBox(   //Use of SizedBox 
                height: 20, 
              ),
              Text(
                "TEAM 2",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.orange[900]),
              ),
              SizedBox(   //Use of SizedBox 
                height: 5, 
              ),
              Container(
                width: 300.0,
                child: TextField(
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      borderSide: BorderSide(width: 1, color: Colors.deepOrange),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      borderSide: BorderSide(width: 1, color: Colors.deepOrange),
                    ),
                  ),                                 
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.orange[900],                  
                  )
                )
              ),
              SizedBox(   //Use of SizedBox 
                height: 20, 
              ),
              Container(
                width: 300,
                height: 30,
                child: RaisedButton(
                  onPressed: () {},
                  color: Colors.orange[900],
                  child: Text(
                    "START",
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SecondPage extends StatefulWidget {
  @override
  _SecondPage createState() => _SecondPage();
}

class _SecondPage extends State<SecondPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("TEAM 1"),
                      Text("10", style: TextStyle(fontSize: 30)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("+"),
                          Text("-"),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("TEAM 2"),
                      Text("8", style: TextStyle(fontSize: 30)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("+"),
                          Text("-"),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Text("reset"),
            ],
          ),
        ),
      ),
    );
  }
}